import Container from 'react-bootstrap/Container';
import { Fragment, useContext, useState } from 'react';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { useLocation } from 'react-router-dom';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import AddProductForm from './AddProduct';

export default function AdminButton() {
  // State to store the user information stored in the login page.
  const { user } = useContext(UserContext);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  function updateProduct (e) {
    e.preventDefault();
    console.log("Update Product");
  }

  function showOrders (e) {
    e.preventDefault();
    console.log("Show User Orders");
  }

  function changeUserStatus (e) {
    e.preventDefault();
    console.log("Change User Status");
  }
  
  return (
    <React.Fragment >
      <ButtonGroup aria-label="Button group with four buttons">
        <AddProductForm 
          name={name} 
          setName={setName} 
          description={description} 
          setDescription={setDescription} 
          price={price} 
          setPrice={setPrice}
        />
        <Button variant="secondary" onClick={(e) => showOrders(e)}>
          Show User Orders
        </Button>
        <Button variant="secondary" onClick={(e) => changeUserStatus(e)}>
          Change User Status
        </Button>



        
      </ButtonGroup>
    </React.Fragment>
  );
}