import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Badge } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const { productId } = useParams();
	const { quantity } = useParams();
	const [count, setCount] = useState(1);

//TO DELETE

// function PlusMinusButtons() {
//   const [count, setCount] = useState(0);



//   return (
//     <div>
//       <button onClick={handleMinusClick}>-</button>
//       <span>{count}</span>
//       <button onClick={handlePlusClick}>+</button>
//     </div>
//   );
// }



//



	const checkout2 = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL}/users/checkout2`, {
			method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					productId: productId,
					quantity: 2
				})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Your product is now for checkout!",
					icon: 'success',
					text: 'You can see your items in the checkout list. '
				});

				navigate("/products");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId]);

//1
  const handlePlusClick = () => {
    setCount(count + 1);
  };

  const handleMinusClick = () => {
    if (count > 0) {
      setCount(count - 1);
    }
  };

//1




	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Number of Items</Card.Subtitle>
							<Card.Text>

								<Badge variant={count > 0 ? "success" : "danger"}>{count}</Badge>
							</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" block onClick={() => checkout2(productId)}>Checkout</Button>
								:
								<Link className="btn btn-danger btn-block" to="/login">Log in to Enroll</Link>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>


	)
}