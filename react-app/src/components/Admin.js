import { Card, Button } from 'react-bootstrap';
import { Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import AdminButton from './AdminButton';

export default function AdminCard({productProp}) {

    const [availability, setAvailability] = useState('Available');
    console.log(useState(0));
    const [seats, setSeats] = useState(10);
    const [active, setIsActive] = useState(true);
    const navigate = useNavigate();
    const { user, setUser} = useContext(UserContext); 
    const {name, description, price, _id, isActive} = productProp;
    



    const disableEnableProduct = (productId) => {


        if(isActive){

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title: "Product Disabled",
                    icon: 'success',
                    text: 'Product now is on archive.'
                });

                navigate("/admin");
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }

        })
    } else {


        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
            method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title: "Product Activated",
                    icon: 'success',
                    text: 'You have successfully activated a product'
                });


                navigate("/admin");
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }

        })

    }
    if(isActive) {
      setAvailability('Available');

    } else {
      setAvailability('Not Available');
    }

    //window.location.reload();

}



    const deleteProduct = (productId) => {


        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/delete`, {
            method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === false){
                Swal.fire({
                    title: "Product Disabled",
                    icon: 'success',
                    text: 'Product now is on archive.'
                });

                navigate("/admin");
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }

        })
    }
    
    function trigger(){


    }


    function ChildComponent({  }) {
  return (

    
    <div>

    <button onClick={() => console.log("Excluded button 1 clicked")}>
        Excluded button 1
      </button></div>

    )
}



    useEffect(() => {
  const storedUser = localStorage.getItem('user');
  if (storedUser) {
    setUser(JSON.parse(storedUser));
     }
    }, [setUser]);


  useEffect(() => {
    if(isActive) {
      setAvailability('Available');

    } else {
      setAvailability('Not Available');
    }

    

  }, [isActive, disableEnableProduct, trigger, navigate]);


    return (

        <Row className="mt-3 mb-3">


          <Col xs={12}>


            <Card>
              <Row>
                <Col xs={12} sm={4}>
                  <Card.Body>
                    <Card.Title>{name}</Card.Title>
                  </Card.Body>
                </Col>
                <Col xs={12} sm={4}>
                  <Card.Body>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                  </Card.Body>
                </Col>
                <Col xs={6} sm={2} className="text-center">
                  <Card.Body>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                  </Card.Body>
                </Col>
                <Col xs={6} sm={2} className="text-center">
                  <Card.Body>
                    <Card.Subtitle>Availability:</Card.Subtitle>
                    <Card.Text>{availability}</Card.Text>
                  </Card.Body>
                </Col>
                <Col xs={12} sm={2} className="text-center">
                  <Card.Body>
                    <Card.Subtitle>Actions:</Card.Subtitle>
                    <div className="d-flex justify-content-between">
                      <Link className="btn btn-warning mr-2" to={`/AdminView/${_id}`}>
                        Update
                      </Link>
                      {isActive ? (
                        <Button
                          variant="primary"
                          onClick={() => disableEnableProduct(_id)}
                        >
                          Disable
                        </Button>
                      ) : (
                        <Button
                          variant="primary"
                          onClick={() => disableEnableProduct(_id)}
                        >
                          Enable
                        </Button>
                      )}


                      <Button
                        variant="btn btn-danger mr-2"
                        onClick={() => deleteProduct(_id)}
                      >
                        Delete
                      </Button>

                    </div>
                  </Card.Body>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>


    )
}

AdminCard.propTypes = {
    // The shape method is used to check  if a prop object conforms to a specific shape
    course: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })

     // <Card.Text>Seats: {seats}</Card.Text>
    //<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
}


