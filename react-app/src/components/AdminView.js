import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form, FormControl } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const { productId } = useParams();
	const { quantity } = useParams();
	const [count, setCount] = useState(0);

//TO DELETE

// function PlusMinusButtons() {
//   const [count, setCount] = useState(0);



//   return (
//     <div>
//       <button onClick={handleMinusClick}>-</button>
//       <span>{count}</span>
//       <button onClick={handlePlusClick}>+</button>
//     </div>
//   );
// }



//

		const submitUpdate = (productId) => {

			let newName = handleNameChange;
			let newPrice = handlePriceChange;
			let newDescription = handleDescriptionChange;

			

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					"name": name,
					"description": description,
					"price": price
				})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Product Updated",
					icon: 'success',
					text: 'You have successfully updated the product'
				});

				navigate("/admin");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}



//
const deleteProduct = (productId) => {
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "DELETE",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Product Updated",
					icon: 'success',
					text: 'You have successfully updated the product'
				});

				navigate("/admin");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}


	//




	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId]);

//1


const handleNameChange = (e) => {
		setName(e.target.value);
	};


const handleDescriptionChange = (e) => {
		setDescription(e.target.value);
	};

	const handlePriceChange = (e) => {
		setPrice(e.target.value);
	};




	return(

    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>Update Product Records</Card.Title>

              {/* add text fields for description, price, and quantity */}
              <Card.Subtitle>Name:</Card.Subtitle>
              <FormControl className="mb-3" placeholder="Name" value={name} onChange={(e) => setName(e.target.value)} />
              <Card.Subtitle>Description:</Card.Subtitle>
              <FormControl className="mb-3" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} />
              <Card.Subtitle>Price:</Card.Subtitle>
              <FormControl className="mb-3" placeholder="Price" value={price} onChange={(e) => setPrice(e.target.value)} />

                <Button variant="primary" block onClick={() => submitUpdate(productId)}>UPDATE DETAILS</Button>
              
            </Card.Body>    
          </Card>
        </Col>
      </Row>
    </Container>

	)
}