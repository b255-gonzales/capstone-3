import { Fragment } from 'react';
import Banner from '../components/Banner';
import React from 'react'
import Highlights from '../components/Highlights';

export default function Home() {
	const data = {
    title: "Electronics E-Site",
    content: "Buy affordable and items.",
    destination: "/products",
    label: "Buy Now!"
}

return (
    <React.Fragment >
        <Banner data={data}/>
        <Highlights />
    </React.Fragment>
)


}
